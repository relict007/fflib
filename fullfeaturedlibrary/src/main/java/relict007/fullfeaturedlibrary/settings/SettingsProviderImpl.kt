package relict007.fullfeaturedlibrary.settings

import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.rxkotlin.toCompletable
import relict007.fullfeaturedlibrary.extensions.cache
import relict007.fullfeaturedlibrary.extensions.changeListener

import javax.inject.Singleton


internal class SettingsProviderImpl(private val spSingle: Single<SharedPreferences>) :
    SettingsProvider {

    val spChangeListener = spSingle
        .flatMapPublisher { it.changeListener() }
        .cache(1)
/*
    //temp function to fix build error
    override fun getString(key: String, defValue: String?): String? {
        return sp.getString(key, defValue)
    }

    //temp function to fix build error
    override fun getStringSet(key: String, defValues: MutableSet<String>?): Set<String>? {
        return sp.getStringSet(key, defValues)!!
    }*/

    override fun setBoolean(key: String, value: Boolean): Completable {
        return spSingle.flatMapCompletable {
            { it.edit().putBoolean(key, value).apply() }.toCompletable()
        }
    }

    override fun setLong(key: String, value: Long): Completable {
        return spSingle.flatMapCompletable {
            { it.edit().putLong(key, value).apply() }.toCompletable()
        }
    }

    override fun incrementLong(key: String): Completable {
        return getLong(key, 0L)
            .flatMapCompletable { setLong(key, it + 1) }
    }

/*    override fun setString(key: String, value: String?) {
        sp.edit().putString(key, value).apply()
    }*/

    override fun watchBoolean(key: String, defValue: Boolean): Flowable<Boolean> {
        return watchValue(key, defValue, ::getBoolean)
    }

    override fun watchLong(key: String, defValue: Long): Flowable<Long> {
        return watchValue(key, defValue, ::getLong)
    }

    private fun getBoolean(key: String, defValue: Boolean): Single<Boolean> {
        return spSingle.map { it.getBoolean(key, defValue) }
    }

    private fun getLong(key: String, defValue: Long): Single<Long> {
        return spSingle.map { it.getLong(key, defValue) }
    }

    private fun <T : Any> watchValue(
        key: String,
        defValue: T,
        getter: (key: String, defValue: T) -> Single<T>
    ): Flowable<T> {
        return spChangeListener
            .filter { it == key }
            .flatMapSingle { getter(it, defValue) }
            .startWith(getter(key, defValue).toFlowable())
            .distinctUntilChanged()
    }
}

@Module
class SettingsProviderModule {

    @Provides
    @Singleton
    fun provideSettingsProvider(spSingle: Single<SharedPreferences>): SettingsProvider =
        SettingsProviderImpl(spSingle)

}