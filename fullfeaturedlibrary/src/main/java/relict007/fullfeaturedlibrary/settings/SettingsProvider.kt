package relict007.fullfeaturedlibrary.settings

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


interface SettingsProvider {

    fun setLong(key: String, value: Long): Completable

    fun incrementLong(key: String): Completable

    fun setBoolean(key: String, value: Boolean): Completable

    fun watchBoolean(key: String, defValue: Boolean): Flowable<Boolean>

    fun watchLong(key: String, defValue: Long): Flowable<Long>

}

//typealias AfterChangeCallback<T> = (oldValue: T?, newValue: T?) -> Unit


/*open class ValueSetting<T>(
    private val name: String,
    private val defValue: T?,
    private val getter: (name: String, defValue: T?) -> T?,
    private val setter: (name: String, value: T?) -> Unit,
    private val afterChangeCallback: AfterChangeCallback<T>? = null
) : ReadWriteProperty<Any?, T?> {

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T?) {
        val oldValue = getter(name, defValue)
        setter(name, value)
        afterChangeCallback?.invoke(oldValue, value)
    }

    override fun getValue(thisRef: Any?, property: KProperty<*>): T? {
        return getter(name, defValue)
    }

}*/

/*class NumberValueSetting<T : Number>(
    private val name: String,
    private val defValue: T,
    private val getter: (name: String, defValue: T) -> T,
    private val setter: (name: String, value: T) -> Unit,
    afterChangeCallback: AfterChangeCallback<T>? = null
) : ValueSetting<T>(
    name,
    defValue,
    { _, _ -> getter(name, defValue) },
    { _, value -> setter(name, value!!) },
    afterChangeCallback
) {
    override fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return getter(name, defValue)
    }
}

fun SettingsProvider.string(
    name: String,
    afterChangeCallback: AfterChangeCallback<String>? = null
) = ValueSetting(
    name,
    null,
    ::getString,
    ::setString,
    afterChangeCallback
)

fun SettingsProvider.long(
    name: String,
    afterChangeCallback: AfterChangeCallback<Long>? = null
) = NumberValueSetting(
    name,
    0L,
    ::getLong,
    ::setLong,
    afterChangeCallback
)*/
