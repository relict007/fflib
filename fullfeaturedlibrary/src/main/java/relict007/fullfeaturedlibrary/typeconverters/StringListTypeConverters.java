package relict007.fullfeaturedlibrary.typeconverters;

import android.util.Log;

import androidx.annotation.Nullable;
import androidx.room.TypeConverter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by saurabh
 */

public class StringListTypeConverters {

    @TypeConverter
    public static List<String> stringToStringList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }
        return splitToStringList(data);
    }

    @TypeConverter
    public static String stringListToString(List<String> strings) {
        return joinIntoString(strings);
    }


    @Nullable
    private static List<String> splitToStringList(@Nullable String input) {
        if (input == null) {
            return null;
        }
        List<String> result = new ArrayList<>();
        StringTokenizer tokenizer = new StringTokenizer(input, " ");
        while (tokenizer.hasMoreElements()) {
            final String item = tokenizer.nextToken();
            try {
                result.add(item);
            } catch (NumberFormatException ex) {
                Log.e("ROOM", "Malformed integer list", ex);
            }
        }
        return result;
    }

    @Nullable
    private static String joinIntoString(@Nullable List<String> input) {
        if (input == null) {
            return null;
        }

        final int size = input.size();
        if (size == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            sb.append(input.get(i));
            if (i < size - 1) {
                sb.append(" ");
            }
        }
        return sb.toString();
    }

}
