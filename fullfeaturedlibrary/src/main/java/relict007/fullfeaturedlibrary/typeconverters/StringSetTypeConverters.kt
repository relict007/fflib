package relict007.fullfeaturedlibrary.typeconverters

import androidx.room.TypeConverter
import java.util.*

class StringSetTypeConverters {

    @TypeConverter
    fun stringToStringSet(data: String?): Set<String>? {
        return if (data == null) {
            emptySet()
        } else splitToStringSet(data)
    }

    @TypeConverter
    fun stringSetToString(strings: Set<String>?): String? {
        return joinIntoString(strings)
    }

    private fun splitToStringSet(input: String?): Set<String>? {
        if (input == null) {
            return null
        }
        val result = mutableSetOf<String>()
        val tokenizer = StringTokenizer(input, " ")
        while (tokenizer.hasMoreElements()) {
            val item = tokenizer.nextToken()
            try {
                result.add(item)
            } catch (ex: NumberFormatException) {

            }
        }
        return result
    }

    private fun joinIntoString(input: Set<String>?): String? {
        if (input == null) {
            return null
        }
        val size = input.size
        if (size == 0) {
            return ""
        }
        val sb = StringBuilder()
        for (s in input) {
            sb.append(s)
            sb.append(" ")
        }
        return sb.toString().trimEnd()
    }


}