package relict007.fullfeaturedlibrary.dagger.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import dagger.Lazy

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjectionModule
import io.reactivex.Single

/**
 * Created by saurabh
 */

@Module(includes = [AndroidInjectionModule::class])
class ApplicationModule(private val application: Application) {


    @Singleton
    @Provides
    fun provideApplication(): Application {
        return application
    }

    @Singleton
    @Provides
    fun provideContext(): Context {
        return application
    }

    @Singleton
    @Provides
    internal fun provideDefaultSharedPreferences(): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(application)
    }

    @Singleton
    @Provides
    fun provideDefaultSharedPreferencesSingle(lazy: Lazy<SharedPreferences>): Single<SharedPreferences> {
        return Single.fromCallable { lazy.get() }.cache()
    }


}
