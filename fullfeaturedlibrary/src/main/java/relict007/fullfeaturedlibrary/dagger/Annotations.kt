package relict007.fullfeaturedlibrary.dagger

import javax.inject.Qualifier
import javax.inject.Scope

/**
 * Created by saurabh.
 */

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class PerService

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class PerFragment

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class PerBroadcastReceiver

@Qualifier
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class AppVersionCode

@Qualifier
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class AppVersionName

@Qualifier
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class ApplicationId