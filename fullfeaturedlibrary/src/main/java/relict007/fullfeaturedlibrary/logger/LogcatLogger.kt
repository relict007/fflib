package relict007.fullfeaturedlibrary.logger

import android.util.Log
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoSet
import javax.inject.Singleton

internal class LogcatLogger : ILogger {
    override fun v(tag: String, message: String) {
        Log.v(tag, message)
    }

    override fun e(tag: String, message: String) {
        Log.e(tag, message)
    }

    override fun d(tag: String, message: String) {
        Log.d(tag, message)
    }

    override fun e(tag: String, message: String, throwable: Throwable) {
        Log.e(tag, message, throwable)
    }

}

@Module
class LogcatLoggerModule {

    @Singleton
    @IntoSet
    @Provides
    fun provideLogcatLogger(): ILogger {
        return LogcatLogger()
    }
}