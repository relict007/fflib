package relict007.fullfeaturedlibrary.logger

import dagger.Module
import dagger.Provides
import dagger.multibindings.ElementsIntoSet
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class Logger @Inject constructor(private val loggers: Set<@JvmSuppressWildcards ILogger>) {

    fun v(tag: String, message: String) {
        loggers.forEach { logger -> logger.v(tag, message) }
    }

    fun d(tag: String, message: String) {
        loggers.forEach { logger -> logger.d(tag, message) }
    }

    @JvmOverloads
    fun e(tag: String, message: String, throwable: Throwable? = null) = loggers.forEach {
        when (throwable) {
            null -> it.e(tag, message)
            else -> it.e(tag, message, throwable)
        }
    }
}

@Module
class EmptyLoggerModule() {

    @Singleton
    @ElementsIntoSet
    @Provides
    fun provideEmptyModule(): Set<ILogger> {
        return emptySet()
    }

}