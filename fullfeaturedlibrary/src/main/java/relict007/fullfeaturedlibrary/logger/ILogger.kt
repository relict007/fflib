package relict007.fullfeaturedlibrary.logger

/**
 * Created by saurabh
 */

interface ILogger {
    fun v(tag: String, message: String)
    fun d(tag: String, message: String)
    fun e(tag: String, message: String)
    fun e(tag: String, message: String, throwable: Throwable)
}
