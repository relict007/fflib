package relict007.fullfeaturedlibrary.architecturecomponent

import androidx.annotation.CallSuper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import relict007.fullfeaturedlibrary.extensions.postValue

/**
 * View Model extra features to support command/action model.
 * @property bgScope Make sure you use this with a single thread backed dispather. Everything goes
 * for a toss if you pass a dispatcher which has more than 1 thread in its pool
 * @constructor
 */
abstract class ExtendedViewModel(
    private val bgScope: CoroutineScope
) : ViewModel() {

    private lateinit var job: Job
    private val myScope: CoroutineScope by lazy {
        job = Job()
        bgScope + job
    }


    private val _actions by lazy { MutableLiveData<Event<ViewModelAction>>() }
    val actions: LiveData<Event<ViewModelAction>> by lazy { _actions }

    protected fun postAction(action: ViewModelAction) {
        _actions.postValue = Event(action)
    }

    fun enqueueWork(work: () -> Unit) {
        myScope.launch {
            work()
        }
    }

    @CallSuper
    override fun onCleared() {
        if (::job.isInitialized) job.cancel()
    }
}


/**
 * an action that this viewmodel expects lifecycle owner to perform
 */
interface ViewModelAction

typealias ActionsHandler = @JvmSuppressWildcards(true) (actionEvent: LiveData<Event<ViewModelAction>>) -> Unit
