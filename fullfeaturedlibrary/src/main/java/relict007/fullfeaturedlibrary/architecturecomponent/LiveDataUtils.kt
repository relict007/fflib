package relict007.fullfeaturedlibrary.architecturecomponent

import androidx.lifecycle.MutableLiveData

fun MutableLiveData<Boolean>.toggle(): Unit {
    value?.let {
        postValue(!it)
    }
}