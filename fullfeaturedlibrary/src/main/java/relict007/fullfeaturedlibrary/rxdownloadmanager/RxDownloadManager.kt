package relict007.fullfeaturedlibrary.rxdownloadmanager

import android.app.DownloadManager
import android.content.Context
import dagger.Module
import dagger.Provides
import io.reactivex.Flowable
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by saurabh.
 */
class RxDownloadManager constructor(
    private val downloadManager: DownloadManager,
    private val pollDelayMS: Long = 2000
) {

    /**
     * enqueue a new download
     * @param request download request
     * @return a Single which emits the download id
     */
    fun enqueue(request: DownloadManager.Request): Single<Long> {
        return Single.fromCallable { downloadManager.enqueue(request) }
    }

    /**
     * remove download(s)
     * @param ids download ids provided earlier by [enqueue]
     */
    fun remove(vararg ids: Long): Single<Int> {
        return Single.fromCallable { downloadManager.remove(*ids) }
    }

    /**
     * get download status
     */
    fun downloadStatus(id: Long): Flowable<DownloadStatus> {
        return Flowable.interval(pollDelayMS, TimeUnit.MILLISECONDS)
            .map { getDownloadStatus(id) ?: throw RuntimeException("download status not found") }
    }

    /**
     * check is download exists
     */
    fun downloadExists(id: Long): Single<Boolean> {
        return Single.fromCallable { getDownloadStatus(id) != null }
    }

    private fun getDownloadStatus(id: Long): DownloadStatus? {
        var downloadStatus: DownloadStatus? = null
        val c = downloadManager.query(DownloadManager.Query().setFilterById(id))
        if (c.moveToFirst()) {
            val status = Status.fromInt(
                c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS))
            )!!
            val total = c.getLong(c.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES))
            val complete =
                c.getLong(c.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR))
            var error: Error? = null
            var pauseReason: PauseReason? = null

            if (status == Status.FAILED) {
                error = Error.fromInt(
                    c.getInt(c.getColumnIndex(DownloadManager.COLUMN_REASON))
                )
            } else if (status == Status.PAUSED) {
                pauseReason =
                    PauseReason.fromInt(
                        c.getInt(c.getColumnIndex(DownloadManager.COLUMN_REASON))
                    )
            }
            downloadStatus = DownloadStatus(
                total,
                complete,
                status,
                error,
                pauseReason
            )
        }
        c.close()
        return downloadStatus
    }
}

/**
 * Download status
 */
enum class Status(private val value: Int) {

    FAILED(DownloadManager.STATUS_FAILED),
    PAUSED(DownloadManager.STATUS_PAUSED),
    PENDING(DownloadManager.STATUS_PENDING),
    RUNNING(DownloadManager.STATUS_RUNNING),
    SUCCESSFUL(DownloadManager.STATUS_SUCCESSFUL);

    companion object {
        private val map = Status.values().associateBy(
            Status::value)
        fun fromInt(type: Int) = map[type]
    }

}

/**
 * Download errors
 */
enum class Error(private val value: Int) {

    CANNOT_RESUME(DownloadManager.ERROR_CANNOT_RESUME),
    DEVICE_NOT_FOUND(DownloadManager.ERROR_DEVICE_NOT_FOUND),
    FILE_ALREADY_EXISTS(DownloadManager.ERROR_FILE_ALREADY_EXISTS),
    FILE_ERROR(DownloadManager.ERROR_FILE_ERROR),
    HTTP_DATA_ERROR(DownloadManager.ERROR_HTTP_DATA_ERROR),
    INSUFFICIENT_SPACE(DownloadManager.ERROR_INSUFFICIENT_SPACE),
    TOO_MANY_REDIRECTS(DownloadManager.ERROR_TOO_MANY_REDIRECTS),
    UNHANDLED_HTTP_CODE(DownloadManager.ERROR_UNHANDLED_HTTP_CODE),
    UNKNOWN(DownloadManager.ERROR_UNKNOWN);

    companion object {
        private val map = Error.values().associateBy(
            Error::value)
        fun fromInt(type: Int) = map[type]
    }

}

/**
 * Download pause reasons
 */
enum class PauseReason(private val value: Int) {

    QUEUED_FOR_WIFI(DownloadManager.PAUSED_QUEUED_FOR_WIFI),
    UNKNOWN(DownloadManager.PAUSED_QUEUED_FOR_WIFI),
    WAITING_FOR_NETWORK(DownloadManager.PAUSED_WAITING_FOR_NETWORK),
    WAITING_TO_RETRY(DownloadManager.PAUSED_WAITING_TO_RETRY);

    companion object {
        private val map = PauseReason.values().associateBy(
            PauseReason::value)
        fun fromInt(type: Int) = map[type]
    }

}

data class DownloadStatus(
    val total: Long,
    val completed: Long,
    val status: Status,
    val error: Error? = null,
    val pauseReason: PauseReason? = null
)

@Module
class RxDownloadManagerModule() {

    @Provides
    @Singleton
    fun provideRxDownloadManager(context: Context): RxDownloadManager {
        val downloadManager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        return RxDownloadManager(
            downloadManager,
            2000
        )
    }

}