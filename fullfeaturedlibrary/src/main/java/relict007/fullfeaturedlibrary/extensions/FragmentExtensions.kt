package relict007.fullfeaturedlibrary.extensions

import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.os.Bundle
import android.util.TypedValue
import android.view.ContextThemeWrapper
import androidx.annotation.AttrRes
import androidx.annotation.IdRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity

fun Fragment.getResourceMetadata(key: String): Int {
    return context?.getResourceMetadata(key) ?: 0
}

val Fragment.activityOrThrow: FragmentActivity
    get() = requireActivity()

val Fragment.contextOrThrow: Context
    get() = requireContext()

fun Fragment.getColor(colorResId: Int): Int {
    return ContextCompat.getColor(requireContext(), colorResId)
}

/**
 *
 * @receiver Fragment
 * @param containerId Int: Id of the container where new fragment is supposed to attach
 * @param creator () -> Fragment : Function which will create the new fragment
 * @param tag String? : Optional tag for the fragment
 * @param findPrevious Boolean : Do we want to find the previous fragment and attach that (if possible)
 */
fun Fragment.addFragment(
    @IdRes containerId: Int,
    creator: () -> Fragment,
    tag: String? = null,
    findPrevious: Boolean = false
) {
    childFragmentManager.beginTransaction().apply {
        val fragment = if (findPrevious && tag != null) fragmentManager?.findFragmentByTag(tag)
            ?: creator() else creator()
        replace(containerId, fragment)
        commit()
    }
}

fun Fragment.addFragment(@IdRes containerId: Int, fragment: Fragment) {
    childFragmentManager.beginTransaction().apply {
        replace(containerId, fragment)
        commit()
    }
}

val Fragment.argumentsOrThrow: Bundle
    get() = arguments
        ?: throw IllegalArgumentException("passed arguments are null for fragment ${javaClass.simpleName}")

fun Fragment.themedContext(@AttrRes attr: Int): Context {
    val themeTypeValue = TypedValue()
    requireActivity().theme.resolveAttribute(attr, themeTypeValue, true)
    return ContextThemeWrapper(requireActivity(), themeTypeValue.resourceId)
}

fun Fragment.themedContext(): Context {
    return themedContext(activityOrThrow.theme)
}

fun Fragment.themedContext(theme: Resources.Theme): Context {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        ContextThemeWrapper(activityOrThrow, theme)
    } else {
        activityOrThrow
    }
}