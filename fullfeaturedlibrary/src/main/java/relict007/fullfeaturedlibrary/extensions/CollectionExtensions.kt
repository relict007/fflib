package relict007.fullfeaturedlibrary.extensions

/**
 * Created by saurabh.
 */

fun <T> T.convertToList(): List<T> {
    return listOf(this)
}

fun <T> Iterator<T>.toList(): List<T> =
    ArrayList<T>().apply {
        while (hasNext())
            this += next()
    }

fun <KEY, VALUE> MutableMap<KEY, VALUE>.putSync(key: KEY, value: VALUE) {
    synchronized(this) { put(key, value) }
}

fun <KEY, VALUE> MutableMap<KEY, VALUE>.removeSync(key: KEY) {
    synchronized(this) { remove(key) }
}

inline fun <reified T, reified R : T> Iterable<T>.cast(clazz: Class<R>): List<R> {
    return map { it as R }
}

fun <KEY, VALUE> Map<KEY, VALUE>.getOrError(key: KEY): VALUE {
    return get(key) ?: error("key $key not found in the map")
}

fun <T, R> Collection<T>.mapThis(mapper: (cc: Collection<T>) -> R): R {
    return mapper(this)
}

