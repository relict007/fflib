package relict007.fullfeaturedlibrary.extensions

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.os.IInterface
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable

/**
 * make sure to subscribe to it in ui scheduler
 * @receiver Context
 * @param packageName String
 * @param className String
 * @param serviceMapper Function1<[@kotlin.ParameterName] IBinder, T>
 * @return Flowable<out T>
 */
fun <T : IInterface> Context.connectService(
    packageName: String,
    className: String,
    serviceMapper: (service: IBinder) -> T
): Flowable<T> {
    return Flowable.create({ emitter ->
        val serviceConnection = object : ServiceConnection {
            override fun onServiceDisconnected(name: ComponentName?) {
                emitter.onErrorSafe(RuntimeException("service disconnected"))
            }

            override fun onServiceConnected(name: ComponentName, service: IBinder) {
                emitter.onNext(serviceMapper(service))
            }
        }
        //unbind on dispose
        emitter.onDispose {
            unbindService(serviceConnection)
        }
        val intent = Intent().apply {
            component = ComponentName(packageName, className)
        }
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)

    }, BackpressureStrategy.BUFFER)

}

/**
 * make sure to subscribe to it in ui scheduler
 * @receiver Context
 * @param packageName String
 * @param className String
 * @return Flowable<IBinder>
 */
fun Context.connectService(
    packageName: String,
    className: String
): Flowable<IBinder> {
    return Flowable.create({ emitter ->
        val serviceConnection = object : ServiceConnection {
            override fun onServiceDisconnected(name: ComponentName?) {
                emitter.onErrorSafe(RuntimeException("service disconnected"))
            }

            override fun onServiceConnected(name: ComponentName, service: IBinder) {
                emitter.onNext(service)
            }
        }
        //unbind on dispose
        emitter.onDispose {
            unbindService(serviceConnection)
        }
        val intent = Intent().apply {
            component = ComponentName(packageName, className)
        }
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)

    }, BackpressureStrategy.BUFFER)

}