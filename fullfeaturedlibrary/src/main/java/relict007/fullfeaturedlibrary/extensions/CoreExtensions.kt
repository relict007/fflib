package relict007.fullfeaturedlibrary.extensions


infix fun Boolean?.andSafe(another: Boolean?): Boolean {
    return this ?: false && another ?: false
}

operator fun Boolean?.not(): Boolean {
    return !(this ?: false)
}

/**
 * convertes a number into a [Double]. If not returns 0.0
 * @receiver Any?
 * @return Double
 */
fun Any?.toDoubleDeep(default: Double = 0.0): Double {
    return (this as? Double) ?: (this as? Long)?.toDouble() ?: default
}

fun Any?.toIntDeep(default: Int = 0): Int {
    return (this as? Int) ?: (this as? Long)?.toInt() ?: default
}

fun List<Char>.asString(): String {
    val sb = StringBuilder()
    forEach {
        sb.append(it)
    }
    return sb.toString()
}