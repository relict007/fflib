package relict007.fullfeaturedlibrary.extensions

import android.content.Context
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.util.TypedValue
import androidx.annotation.StringRes
import androidx.annotation.WorkerThread
import arrow.core.*

fun Context.getMetadata(): Bundle {
    return packageManager.getApplicationInfo(
        packageName,
        PackageManager.GET_META_DATA
    ).metaData
}

@WorkerThread
fun Context.getResourceMetadata(key: String): Int? {
    return getMetadata().getInt(key).let { if (it != 0) it else null }
}

fun Context.getDrawableFromMetadata(key: String): Drawable? {
    return getResourceMetadata(key)?.let { getDrawable(it) }
}

fun Context.tryGetString(@StringRes id: Int): String? {
    return Try { getString(id) }.orNull()
}

fun Context.getStringFromMetadata(key: String, @StringRes default: Int = 0): String? {
    return getResourceMetadata(key)?.let { tryGetString(it) }
        .toOption()
        .getOrElse { tryGetString(default) }
}

/*
fun Context.getIntFromMetadata(key: String): Int? {
    return Try { resources.getInteger(getResourceMetadata(key)) }.orNull()
}

fun Context.getBooleanFromMetadata(key: String): Int? {
    return Try { resources.getInteger(getResourceMetadata(key)) }.orNull()
}
*/

fun Context.dpToPx(dp: Int): Int {
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dp.toFloat(),
        resources.displayMetrics
    ).toInt()
}

val Context.versionCode: Try<Long>
    get() {
        return Try {
            val packageInfo = applicationContext.packageManager.getPackageInfo(packageName, 0)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                packageInfo.longVersionCode

            } else {
                packageInfo.versionCode.toLong()
            }
        }
    }
