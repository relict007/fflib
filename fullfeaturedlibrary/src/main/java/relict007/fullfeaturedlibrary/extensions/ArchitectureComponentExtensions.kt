package relict007.fullfeaturedlibrary.extensions

import androidx.annotation.MainThread
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*
import java.lang.IllegalStateException

fun <X, Y> LiveData<X>.map(mapper: (source: X) -> Y): LiveData<Y> {
    return Transformations.map(this, mapper)
}

fun <X> LiveData<X>.combineLatest(
    another: LiveData<X>,
    combiner: (valueFromThis: X?, valueFromAnother: X?) -> X?
): LiveData<X> {
    val mediatorLiveData = MediatorLiveData<X>()
    var valueFromThis: X? = value
    var valueFromAnother: X? = another.value
    val thisObserver = Observer<X> {
        valueFromThis = it
        mediatorLiveData.value = combiner(valueFromThis, valueFromAnother)
    }
    val anotherObserver = Observer<X> {
        valueFromAnother = it
        mediatorLiveData.value = combiner(valueFromThis, valueFromAnother)
    }
    mediatorLiveData.addSource(this, thisObserver)
    mediatorLiveData.addSource(another, anotherObserver)
    return mediatorLiveData
}

/**
 * implements something like RxJava's distinctUntilChanged
 * @receiver LiveData<T>
 * @return LiveData<T>
 */
fun <T> LiveData<T>.distinctUntilChanged(): LiveData<T> {
    val mediatorLiveData = MediatorLiveData<T>()
    val observer = object : Observer<T> {
        var oldValue: T? = null
        override fun onChanged(newValue: T) {
            if (newValue != oldValue) {
                mediatorLiveData.value = newValue
            }
            oldValue = newValue
        }
    }
    mediatorLiveData.addSource(this, observer)
    return mediatorLiveData
}

/**
 * emits the count of items emitted by source [LiveData]
 * @receiver LiveData<T>
 * @return LiveData<T>
 */
fun <T> LiveData<T>.itemsCount(): LiveData<Int> {
    val mediatorLiveData = MediatorLiveData<Int>()
    val observer = object : Observer<T> {
        var count: Int = 0
        override fun onChanged(newValue: T) {
            count++
            mediatorLiveData.value = count
        }
    }
    mediatorLiveData.addSource(this, observer)
    return mediatorLiveData
}

/**
 * filter live data based on [predicate]
 * @receiver LiveData<T>
 * @param predicate (value: T) -> Boolean
 * @return LiveData<T>
 */
fun <T> LiveData<T>.filter(predicate: (value: T) -> Boolean): LiveData<T> {
    val mediatorLiveData = MediatorLiveData<T>()
    val observer = Observer<T> {
        if (predicate.invoke(it)) {
            mediatorLiveData.value = it
        }
    }
    mediatorLiveData.addSource(this, observer)
    return mediatorLiveData
}

/**
 * similar to Rx take item
 * @receiver LiveData<T>
 * @param count Int
 * @return LiveData<T>
 */
fun <T> LiveData<T>.take(count: Int): LiveData<T> {
    val mediatorLiveData = MediatorLiveData<T>()
    val observer = object : Observer<T> {
        private var seenItemsCount: Int = 0
        override fun onChanged(t: T) {
            seenItemsCount++
            if (seenItemsCount <= count) {
                mediatorLiveData.value = t
            } else {
                //TODO should we continue or unsubscribe??
                //mediatorLiveData.removeSource(this@take)
            }
        }
    }
    mediatorLiveData.addSource(this, observer)
    return mediatorLiveData
}



/*inline fun <reified T : ViewModel> FragmentActivity.createViewModel(factory: ViewModelProvider.Factory? = null, key: String? = null): T {
    return ViewModelProviders.of(this, factory).run {
        if (key != null) get(key, T::class.java) else get(T::class.java)
    }
}*/

/*inline fun <reified T : ViewModel> Fragment.createViewModel(
    factory: ViewModelProvider.Factory? = null,
    key: String? = null
): T {
    return ViewModelProviders.of(this, factory).run {
        if (key != null) get(key, T::class.java) else get(T::class.java)
    }
}*/

inline fun <reified T : ViewModel> Fragment.createViewModel(
    key: String? = null,
    crossinline vmCreator: () -> T
): T {
    return ViewModelProviders.of(this, createFactory(vmCreator)).run {
        if (key != null) get(key, T::class.java) else get(T::class.java)
    }
}

inline fun <reified T : ViewModel> Fragment.createViewModelInActivity(
    key: String? = null,
    crossinline vmCreator: () -> T
): T {
    return ViewModelProviders.of(activityOrThrow, createFactory(vmCreator)).run {
        if (key != null) get(key, T::class.java) else get(T::class.java)
    }
}


inline fun <reified T : ViewModel> FragmentActivity.createViewModel(
    key: String? = null,
    crossinline vmCreator: () -> T
): T {
    return ViewModelProviders.of(this, createFactory(vmCreator)).run {
        if (key != null) get(key, T::class.java) else get(T::class.java)
    }
}

/*inline fun <reified T : ViewModel> Fragment.createViewModelInActivity(
    factory: ViewModelProvider.Factory? = null,
    key: String? = null
): T {
    return ViewModelProviders.of(activityOrThrow, factory).run {
        if (key != null) get(key, T::class.java) else get(T::class.java)
    }
}*/

/**
 * create a factory which in turn calls vmCreator to create actual ViewModel
 * @param vmCreator () -> VM
 * @return ViewModelProvider.Factory
 */
inline fun <VM : ViewModel?> createFactory(crossinline vmCreator: () -> VM): ViewModelProvider.Factory {
    return object : ViewModelProvider.Factory {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return vmCreator() as T
        }
    }
}

@MainThread
fun <T, R> LiveData<T>.bind(
    owner: LifecycleOwner,
    call: (value: R) -> Unit,
    mapper: (value: T) -> R
) {
    observe(owner, Observer {
        call(mapper(it))
    })
}

var <T> MutableLiveData<T>.postValue: T?
    get() {
        throw IllegalStateException("cant read value through postValue")
    }
    set(value: T?) {
        postValue(value)

    }