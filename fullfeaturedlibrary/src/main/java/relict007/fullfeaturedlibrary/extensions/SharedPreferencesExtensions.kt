package relict007.fullfeaturedlibrary.extensions

import android.content.SharedPreferences
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable

fun SharedPreferences.changeListener(): Flowable<String> {
    return Flowable.create({ emitter ->
        val listener = SharedPreferences.OnSharedPreferenceChangeListener { _, name ->
            if (!name.isNullOrBlank()) {
                emitter.onNextSafe(name)
            }
        }
        emitter.onDispose {
            unregisterOnSharedPreferenceChangeListener(listener)
        }
        registerOnSharedPreferenceChangeListener(listener)
    }, BackpressureStrategy.BUFFER)
}