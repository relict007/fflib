package relict007.fullfeaturedlibrary.extensions

import android.content.ClipData
import android.content.Context
import android.net.Uri
import android.util.Patterns


fun Uri.getMimeType(context: Context): String? {
    return context.contentResolver.getType(this)
}

/**
 * convert this [String] to [Uri]
 * @receiver String
 * @return Uri
 */
fun String.toUri(): Uri {
    return Uri.parse(this)
}

fun verifyValidUrl(input: String): Boolean {
    return Patterns.WEB_URL.matcher(input).matches()
}

fun ClipData.getAllItems(): List<ClipData.Item> {
    return mutableListOf<ClipData.Item>().apply {
        for (i in 0 until itemCount) {
            add(getItemAt(i))
        }
    }
}

