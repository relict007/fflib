package relict007.fullfeaturedlibrary.extensions

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import arrow.core.Option
import arrow.core.toOption
import io.reactivex.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Function
import org.reactivestreams.Publisher
import relict007.fullfeaturedlibrary.BuildConfig
import java.util.concurrent.TimeUnit

/**
 * Created by saurabh.
 */


/*
timeout only for the 1st emitted item.
@param value Timeout value in milliseconds
 */
fun <T> Flowable<T>.firstItemTimeout(value: Long): Flowable<T> {
    return timeout(
        Flowable.just(1).delay(value, TimeUnit.MILLISECONDS),
        Function<T, Publisher<Long>> { Flowable.never() })
}

fun <T> FlowableEmitter<T>.onDispose(disposeAction: () -> Unit) {
    setDisposable(object : Disposable {
        private var disposed = false
        override fun isDisposed() = disposed
        override fun dispose() {
            disposeAction()
            disposed = true
        }
    })
}

fun <T> FlowableEmitter<T>.isNotCancelled(): Boolean {
    return !isCancelled
}

fun <T> FlowableEmitter<T>.onNextSafe(value: T) {
    if (isNotCancelled()) onNext(value)
}

fun <T> FlowableEmitter<T>.onCompleteSafe() {
    if (isNotCancelled()) onComplete()
}

fun <T> FlowableEmitter<T>.onErrorSafe(throwable: Throwable) {
    if (isNotCancelled()) onError(throwable)
}

fun CompletableEmitter.isNotCancelled(): Boolean {
    return !isDisposed
}

fun CompletableEmitter.onCompleteSafe() {
    if (isNotCancelled()) onComplete()
}

fun CompletableEmitter.onErrorSafe(throwable: Throwable) {
    if (isNotCancelled()) onError(throwable)
}

fun <T> SingleEmitter<T>.onDispose(disposeAction: () -> Unit) {
    setDisposable(object : Disposable {
        private var disposed = false
        override fun isDisposed() = disposed
        override fun dispose() {
            disposeAction()
            disposed = true
        }
    })
}

fun <T> SingleEmitter<T>.isNotDisposed(): Boolean {
    return !isDisposed
}

fun <T> SingleEmitter<T>.onSuccessSafe(value: T) {
    if (isNotDisposed()) onSuccess(value)
}

fun <T> SingleEmitter<T>.onErrorSafe(throwable: Throwable) {
    if (isNotDisposed()) onError(throwable)
}

/**
 * convert a [Flowable] to [Single] which emits only the very 1st item
 * @receiver Flowable<T>
 * @return Single<T>
 */
fun <T> Flowable<T>.toSingle(): Single<T> {
    return take(1)
        .singleOrError()
}

/**
 * counts the emission up-to given "count* number of times and then completes
 * @receiver Flowable<T>
 * @param count Int
 * @return Completable
 */
fun <T> Flowable<T>.maxCount(count: Int): Completable {
    return distinctUntilChanged()
        .take(2)
        .count()
        .ignoreElement()
}

fun <T> Single<T>.toOptionItem(): Single<Option<T>> {
    return map { it.toOption() }
        .onErrorReturn { Option.empty() }
}


fun <T, R> Flowable<out Iterable<T>>.mapItems(mapper: (value: T) -> R): Flowable<List<R>> {
    return map { collection -> collection.map { mapper(it) } }
}

//TODO have a timeout here so that we can cache content a little while longer
/**
 * cache only the last [count] elements rather than all
 * https://medium.com/@elye.project/rxjava-clean-way-of-prefetching-data-and-use-later-54800f2652d4
 * @receiver Flowable<T>
 * @param count Int
 * @return Flowable<T>
 */
fun <T> Flowable<T>.cache(
    count: Int,
    timeout: Long = 0,
    timeUnit: TimeUnit = TimeUnit.MILLISECONDS
): Flowable<T> {
    /*
    we can also implement the same using
    replay(1).autoConnect()
    However, autoConnect() doesn't unsubscribe the source even after all the downstream subscriptions
    are disconnected. This leads to memory leak
     */
    return replay(count).refCount(timeout, timeUnit)
}


/**
 * converts an action lambda to completable. The action runs in computation() scheduler.
 * @receiver (() -> Any?)
 * @return Completable
 */
fun (() -> Any?).toCompletableWithDelay(
    delay: Long = 0,
    timeUnit: TimeUnit = TimeUnit.MILLISECONDS
): Completable {
    return Completable.complete()
        .delay(delay, timeUnit)
        .andThen(Completable.fromAction { invoke() })
}

fun <T> (() -> Throwable).toErrorFlowable(
    delay: Long = 0,
    timeUnit: TimeUnit = TimeUnit.MILLISECONDS
): Flowable<T> {
    return Completable.complete()
        .delay(delay, timeUnit)
        .andThen(Flowable.error<T>(invoke()))
}

fun <T> (() -> T).toSingle(): Single<T> {
    return Single.fromCallable { invoke() }
}

/**
 * dispose if not already disposed otherwise noop
 * @receiver Disposable
 */
fun Disposable?.disposeSafe() {
    if (this != null && !isDisposed) dispose()
}

fun <T> Single<T>.logSuccess(message: String, tag: String? = null): Single<T> {
    return doOnSuccess { Log.d("SingleSuccess-->" + (tag ?: ""), "$message value: $it") }
}

fun <T> Single<T>.logSubscribe(message: String, tag: String? = null): Single<T> {
    return doOnSubscribe { Log.d("SingleSubscribe-->" + (tag ?: ""), message) }
}

fun <T> Single<T>.logDispose(message: String, tag: String? = null): Single<T> {
    return doOnDispose { Log.d("SingleDispose-->" + (tag ?: ""), message) }
}

fun <T> Single<T>.logError(message: String, tag: String? = null): Single<T> {
    return doOnError { Log.e("SingleError-->" + (tag ?: ""), "$message, Exception: $it") }
}

fun <T> Single<T>.logAll(message: String, tag: String? = null): Single<T> {
    return logSuccess(message, tag)
        .logSubscribe(message, tag)
        .logError(message, tag)
        .logDispose(message, tag)
}


fun <T> Flowable<T>.logNext(message: String, tag: String? = null): Flowable<T> {
    return doOnNext { Log.d("FlowableNext-->" + (tag ?: ""), "$message  value: $it") }
}

fun <T> Flowable<T>.logSubscribe(message: String, tag: String? = null): Flowable<T> {
    return doOnSubscribe { Log.d("FlowableSubscribe-->" + (tag ?: ""), message) }
}

fun <T> Flowable<T>.logCancel(message: String, tag: String? = null): Flowable<T> {
    return doOnCancel { Log.d("FlowableCancel-->" + (tag ?: ""), message) }
}

fun <T> Flowable<T>.logTerminate(message: String, tag: String? = null): Flowable<T> {
    return doOnTerminate { Log.d("FlowableTerminate-->" + (tag ?: ""), message) }
}

fun <T> Flowable<T>.logError(message: String, tag: String? = null): Flowable<T> {
    return doOnError {
        Log.d("FlowableError-->" + (tag ?: ""), "$message, Exception $it")
        it.printStackTrace()
    }
}

fun <T> Flowable<T>.logAll(message: String, tag: String? = null): Flowable<T> {
    return logNext(message, tag)
        .logCancel(message, tag)
        .logError(message, tag)
        .logSubscribe(message, tag)
        .logTerminate(message, tag)
}

fun Completable.logTerminate(message: String, tag: String? = null): Completable {
    return doOnTerminate { Log.d("CompletableTerminate-->" + (tag ?: ""), message) }
}

fun Completable.logComplete(message: String, tag: String? = null): Completable {
    return doOnComplete { Log.d("CompletableComplete-->" + (tag ?: ""), message) }
}

fun Completable.logSubscribe(message: String, tag: String? = null): Completable {
    return doOnSubscribe { Log.d("CompletableSubscribe-->" + (tag ?: ""), message) }
}

fun Completable.logError(message: String, tag: String? = null): Completable {
    return doOnError { Log.d("CompletableError-->" + (tag ?: ""), "$message, Exception $it") }
}

fun Completable.logAll(message: String, tag: String? = null): Completable {
    return logComplete(message, tag)
        .logError(message, tag)
        .logSubscribe(message, tag)
        .logTerminate(message, tag)
}

fun <T> Flowable<T>.toLiveData(defaultItem: T? = null): LiveData<T> {
    return LiveDataReactiveStreams.fromPublisher(
        if (defaultItem != null)
            onErrorReturnItem(defaultItem) else this
    )
}

fun <T> Single<T>.toLiveData(): LiveData<T> {
    return LiveDataReactiveStreams.fromPublisher(toFlowable())
}