package relict007.fullfeaturedlibrary.extensions

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.navigation.NavController
import arrow.core.Try

fun NavController.isGraphInitialized(): Boolean {
    return Try { graph }.isSuccess()
}

/**
 * verify if [resId] action can be taken safely
 * @receiver NavController
 * @param resId Int
 * @return Boolean
 */
fun NavController.verifyNavigation(@IdRes resId: Int): Boolean {
    return currentDestination?.getAction(resId) != null
}

