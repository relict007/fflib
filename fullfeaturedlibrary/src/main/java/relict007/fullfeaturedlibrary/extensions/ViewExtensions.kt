package relict007.fullfeaturedlibrary.extensions

import android.os.Bundle
import android.view.View
import relict007.fullfeaturedlibrary.extensions.dpToPx
import relict007.fullfeaturedlibrary.extensions.getMetadata

fun View.getMetadata(): Bundle {
    return context.getMetadata()
}

fun View.dpToPx(dp: Int): Int = context.dpToPx(dp)