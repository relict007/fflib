package relict007.fullfeaturedlibrary.extensions

import java.util.*

val GregorianCalendar.year
    get() = get(Calendar.YEAR)

val GregorianCalendar.minute
    get() = get(Calendar.MINUTE)

val GregorianCalendar.dayOfYear
    get() = get(Calendar.DAY_OF_YEAR)

val GregorianCalendar.weekOfYear
    get() = get(Calendar.WEEK_OF_YEAR)

val GregorianCalendar.dayOfMonth
    get() = get(Calendar.DAY_OF_MONTH)

val GregorianCalendar.dayOfWeek
    get() = get(Calendar.DAY_OF_WEEK)

val GregorianCalendar.hourOfDay
    get() = get(Calendar.HOUR_OF_DAY)

val GregorianCalendar.hour
    get() = get(Calendar.HOUR)

val GregorianCalendar.month
    get() = get(Calendar.MONTH)