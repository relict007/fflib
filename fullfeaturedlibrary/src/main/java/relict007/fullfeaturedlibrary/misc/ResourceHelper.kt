package relict007.fullfeaturedlibrary.misc

import android.content.Context
import androidx.core.content.ContextCompat
import dagger.Module
import dagger.Provides
import relict007.fullfeaturedlibrary.BuildConfig
import relict007.fullfeaturedlibrary.extensions.getResourceMetadata
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ResourceHelper @Inject constructor(private val context: Context) {

    private fun getResourceMetadata(key: String): Int? {
        return context.getResourceMetadata(key)
    }

    fun getString(value: Int): String {
        return context.getString(value)
    }

    fun getString(key: String, debug: Boolean = false): String? {
        return getResourceMetadata(key).let {
            when {
                it != null && it > 0 -> context.getString(it)
                debug -> error("metadata $key not set")
                else -> null
            }
        }
    }
}

@Module
class ResourceHelperModule {

    @Provides
    @Singleton
    fun provideResourceHelper(context: Context): ResourceHelper {
        return ResourceHelper(context)
    }

}