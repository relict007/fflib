package relict007.fullfeaturedlibrary.misc

import android.os.Build
import java.time.LocalDateTime
import java.util.*

/**
 * return hour of the day in 24 hour format
 */
fun getHourOfTheDay(): Int {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        LocalDateTime.now().hour
    } else {
        Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
    }
}

fun getMinuteOfTheHour(): Int {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        LocalDateTime.now().minute
    } else {
        Calendar.getInstance().get(Calendar.MINUTE)
    }
}

val CURRENT_TIME_MILLIS: Long
    get() = System.currentTimeMillis()