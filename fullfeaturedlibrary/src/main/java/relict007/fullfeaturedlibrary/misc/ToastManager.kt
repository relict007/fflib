package relict007.fullfeaturedlibrary.misc

import android.content.Context
import android.widget.Toast
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import javax.inject.Singleton

class ToastManager constructor(
    private val context: Context,
    private val schedulers: RxSchedulers
) {
    private lateinit var job: Job
    private val myScope: CoroutineScope by lazy {
        job = Job()
        schedulers.scopes.ui + job
    }

    private fun enqueueWork(work: () -> Unit) {
        myScope.launch {
            work()
        }
    }


    fun showText(text: String, long: Boolean = true) {
        enqueueWork {
            Toast.makeText(context, text, if (long) Toast.LENGTH_LONG else Toast.LENGTH_SHORT)
                .show()
        }


    }
}

@Module
class ToastManagerModule {

    @Provides
    @Singleton
    fun provideToastManager(context: Context, schedulers: RxSchedulers): ToastManager {
        return ToastManager(context, schedulers)
    }

}