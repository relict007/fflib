package relict007.fullfeaturedlibrary.misc

import android.content.Context
import android.net.ConnectivityManager
import androidx.core.content.ContextCompat.getSystemService


fun isNetworkAvailable(context: Context): Boolean {
    return getSystemService(
        context,
        ConnectivityManager::class.java
    )?.activeNetworkInfo?.isConnected
        ?: false
}