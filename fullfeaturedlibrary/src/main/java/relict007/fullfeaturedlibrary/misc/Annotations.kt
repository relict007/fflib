package relict007.fullfeaturedlibrary.misc

@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION,
    AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.EXPRESSION)
@MustBeDocumented
@Retention(AnnotationRetention.SOURCE)
annotation class OnlyForConvenience(
    val reason: String = "This is provided only for convenience thinking of most frequent use cases." +
            "However, if need arises, feel free to ignore this and use your own implementation")

