package relict007.fullfeaturedlibrary.misc

import android.os.Build
import android.os.Handler
import android.os.Looper
import androidx.annotation.VisibleForTesting
import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import kotlinx.coroutines.android.asCoroutineDispatcher
import relict007.fullfeaturedlibrary.BuildConfig
import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.ThreadFactory
import kotlin.random.Random

/**
 * Created by saurabh.
 */

interface RxSchedulers {
    val ui: Scheduler
    val io: Scheduler
    val computation: Scheduler
    val single: Scheduler
    val viewModelIO: Scheduler
    val bgIO: Scheduler
    val new: Scheduler
    val newSingle: Scheduler
    val trampoline: Scheduler
    val singleA: Scheduler
    val singleB: Scheduler
    val singleBG: Scheduler
    val scopes: CoroutineScopes
}

interface CoroutineScopes {
    val ui: CoroutineScope
    val viewModelActions: CoroutineScope
    val single1: CoroutineScope
    val single2: CoroutineScope
    val single3: CoroutineScope
    val single4: CoroutineScope
}

/**
 *
 * @param isAndroid whether we are running in Android environment.Only time it would be false is in local unit tests
 * @return RxSchedulers
 */
@VisibleForTesting
fun createSchedulers(isAndroid: Boolean = true): RxSchedulers {
    return object : RxSchedulers {

        private val singleExecutor: Executor by lazy { createThreadPool(1, "single") }

        override val ui: Scheduler by lazy { if (isAndroid) AndroidSchedulers.mainThread() else new }
        override val io: Scheduler by lazy { Schedulers.io() }
        override val computation: Scheduler by lazy { Schedulers.computation() }
        override val single: Scheduler by lazy { singleExecutor.asScheduler() }
        override val viewModelIO: Scheduler by lazy { createScheduler(4, "viewModelIO") }
        override val bgIO: Scheduler by lazy { createScheduler(4, "bgIO") }
        override val new: Scheduler
            get() = Schedulers.newThread()
        override val newSingle: Scheduler
            get() = createScheduler(1, "newSingle")
        override val trampoline: Scheduler by lazy { Schedulers.trampoline() }
        //singleA is mostly for singleton bg components like analytics and stats
        override val singleA: Scheduler by lazy { single }
        override val singleB: Scheduler by lazy { createScheduler(1, "singleB") }
        override val singleBG: Scheduler by lazy { createScheduler(1, "singleBG") }

        override val scopes: CoroutineScopes by lazy {
            object : CoroutineScopes {

                //single1 is mostly for singleton bg components like analytics and stats
                override val single1: CoroutineScope by lazy {
                    CoroutineScope(singleExecutor.asCoroutineDispatcher())
                }

                //single1 is mostly for singleton bg components like analytics and stats
                override val single2: CoroutineScope by lazy {
                    CoroutineScope(singleExecutor.asCoroutineDispatcher())
                }

                //single1 is mostly for singleton bg components like analytics and stats
                override val single3: CoroutineScope by lazy {
                    CoroutineScope(singleExecutor.asCoroutineDispatcher())
                }

                //single1 is mostly for singleton bg components like analytics and stats
                override val single4: CoroutineScope by lazy {
                    CoroutineScope(singleExecutor.asCoroutineDispatcher())
                }


                override val ui: CoroutineScope by lazy {
                    //check this thread on why we are not using Dispatchers.Main here
                    //https://github.com/Kotlin/kotlinx.coroutines/issues/878
                    CoroutineScope(
                        if (isAndroid) {
                            Handler(Looper.getMainLooper()).asCoroutineDispatcher("UI")
                        } else {
                            createSingleDispatcher("Main")
                        }
                    )
                }

                override val viewModelActions: CoroutineScope by lazy {
                    CoroutineScope(
                        createSingleDispatcher("viewModelActions")
                    )
                }

            }
        }

    }

}

@Module
class SchedulersModule {

    @Singleton
    @Provides
    fun provideRxSchedulers(): RxSchedulers {
        return createSchedulers()
    }
}

private fun createSingleDispatcher(name: String): CoroutineDispatcher {
    return createThreadPool(1, name).asCoroutineDispatcher()
}

private fun createScheduler(count: Int, name: String = "default"): Scheduler {
    return Schedulers.from(createThreadPool(count, name))
}

private fun Executor.asScheduler(): Scheduler {
    return Schedulers.from(this)
}

private fun createThreadPool(count: Int, name: String, debug: Boolean = false): ExecutorService {
    /**
     * TODO verify that newWorkStealingPool has better performance than newFixedThreadPool
     * this deletes the threads and creates them again which might have some adverse effect
     * on the performance
     */
    return when {
        debug -> Executors.newFixedThreadPool(count, NamedThreadFactory(name))
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.N -> Executors.newWorkStealingPool(count)
        else -> Executors.newFixedThreadPool(count)
    }
}

private class NamedThreadFactory(
    private val threadNamePrefix: String,
    private val threadFactory: ThreadFactory = Executors.defaultThreadFactory()
) : ThreadFactory by threadFactory {
    override fun newThread(r: Runnable?): Thread {
        return threadFactory.newThread(r).apply {
            name = "${threadNamePrefix}_${Random.nextInt(1000)}"
        }
    }

}