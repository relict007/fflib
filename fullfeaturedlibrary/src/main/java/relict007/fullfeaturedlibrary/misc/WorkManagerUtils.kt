package relict007.fullfeaturedlibrary.misc

import android.content.Context
import androidx.work.ListenableWorker
import androidx.work.WorkManager
import androidx.work.WorkerFactory
import androidx.work.WorkerParameters
import androidx.work.Configuration

interface CustomWorkerBuilder {
    fun createWorker(appContext: Context, workerParameters: WorkerParameters): ListenableWorker
}

class CustomWorkerFactory constructor(private val builders: Map<@JvmSuppressWildcards String, @JvmSuppressWildcards CustomWorkerBuilder>) :
    WorkerFactory() {

    override fun createWorker(
        appContext: Context,
        workerClassName: String,
        workerParameters: WorkerParameters
    ): ListenableWorker? {
        return builders[workerClassName]?.createWorker(appContext, workerParameters)
    }

}

fun Context.initWorkManager(builders: Map<String, CustomWorkerBuilder>) {
    WorkManager.initialize(
        applicationContext,
        Configuration.Builder()
            .setWorkerFactory(CustomWorkerFactory(builders))
            .build()
    )
}