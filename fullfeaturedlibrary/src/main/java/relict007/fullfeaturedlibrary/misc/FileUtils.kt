package relict007.fullfeaturedlibrary.misc

import android.content.Context
import android.os.Environment
import android.os.Environment.MEDIA_MOUNTED
import android.os.Environment.MEDIA_MOUNTED_READ_ONLY
import java.io.File
import java.net.URI

/**
 * Created by saurabh.
 */
fun isFileReadable(context: Context, uri: String): Boolean {
    return when {
        uri.startsWith("asset:///", true) -> checkAssetExists(
            context,
            uri
        )
        uri.startsWith("file:///", true) -> checkFileReadable(uri)
        else -> false
    }

}


fun checkAssetExists(context: Context, assetPath: String): Boolean {
    // FIXME: do we close the assetManager here in finally(was causing exception elsewhere) ???
    return try {
        with(context.assets.open(assetPath.replace("asset:///", ""))) { close() }
        true
    } catch (ex: Exception) {
        ex.printStackTrace()
        false
    }
}

// TODO: also check permissions here
// TODO: this is not yet complete
fun checkFileReadable(filePath: String): Boolean {
    // FIXME: this is returning true for all file:/// paths
    return try {
        with(File(URI(filePath))) {
            exists() && canRead()
        }
    } catch (ex: Exception) {
        // TODO: handle permissions here
        ex.printStackTrace()
        false
    }

}

fun isExternalStorageAvailable(): Boolean {
    return Environment.getExternalStorageState().run {
        this == MEDIA_MOUNTED || this == MEDIA_MOUNTED_READ_ONLY
    }
}

fun isExternalStorageWritable(): Boolean {
    return Environment.getExternalStorageState().run {
        this == MEDIA_MOUNTED
    }
}
