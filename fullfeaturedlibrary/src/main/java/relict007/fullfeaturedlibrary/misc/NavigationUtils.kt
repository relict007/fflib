package relict007.fullfeaturedlibrary.misc

import androidx.navigation.NavController
import androidx.navigation.NavDestination
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import relict007.fullfeaturedlibrary.extensions.onDispose
import relict007.fullfeaturedlibrary.extensions.onNextSafe

fun NavController.navigationListener(): Flowable<Int> {
    return Flowable.create({ emitter ->
        val listener =
            NavController.OnDestinationChangedListener { _, destination, _ ->
                emitter.onNextSafe(destination.id)
            }

        addOnDestinationChangedListener(listener)
        emitter.onDispose {
            removeOnDestinationChangedListener(listener)
        }

    }, BackpressureStrategy.BUFFER)
}