package relict007.fullfeaturedlibrary.misc

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.AudioManager.*
import android.net.Uri
import android.provider.Settings
import androidx.core.content.FileProvider
import io.reactivex.Completable
import io.reactivex.Single
import relict007.fullfeaturedlibrary.extensions.getResourceMetadata
import relict007.fullfeaturedlibrary.extensions.getStringFromMetadata
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class AndroidHelper @Inject constructor(private val context: Context) {

    /**
     * copy a file in the app's private cache directory
     * @param file source file
     * @param name new name of the file
     * @param dirName option sub-directory name
     * @param overwrite overwrite the existing file
     */
    fun copyFile(
        file: File,
        name: String,
        dirName: String? = null,
        overwrite: Boolean = true
    ): Single<File> {
        return Single.fromCallable {
            val dir = dirName?.let {
                File(context.externalCacheDir, it).apply {
                    mkdir()
                }
            }
            val newFile = dir?.let {
                File(dir, name)
            } ?: File(context.externalCacheDir, name)
            file.copyTo(newFile, overwrite)
        }
    }

    fun getShareableUri(file: File, authority: String? = null): String {
        return FileProvider.getUriForFile(
            context,
            authority ?: context.packageName, //assuming authority is same as packagename
            file
        ).toString()
    }

    val networkAvailable: Boolean
        get() = isNetworkAvailable(context)

    fun copyContentUri(
        uri: String,
        name: String,
        dir: File
    ): Completable {
        return Completable.fromAction {
            //File(Uri.parse(uri).path).copyTo(File(dir, name), overwrite)
            val inputStream = context.contentResolver.openInputStream((Uri.parse(uri)))
            val outputStream = FileOutputStream(File(dir, name))
            inputStream?.copyTo(outputStream)
            inputStream?.close()
            outputStream.close()


            //context.contentResolver.openInputStream((Uri.parse(uri)))?.copyTo(FileOutputStream(File(dir, name), false))
        }
    }

    fun loadAssetFile(name: String): InputStream? {
        return try {
            context.assets.open(name)
        } catch (ex: Exception) {
            null
        }
    }

    val deviceModel: String = android.os.Build.MODEL

    fun getStringFromMetadata(key: String): String? {
        return context.getStringFromMetadata(key, 0)
    }

    fun getResourceMetadata(key: String): Int? {
        return context.getResourceMetadata(key)
    }

    /**
     *
     * Assumes that you have the permission
     * @param value Brightness value 0-255
     */
    fun setBrightness(value: Int) {
        Settings.System.putInt(
            context.contentResolver,
            Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL
        )
        Settings.System.putInt(
            context.contentResolver,
            Settings.System.SCREEN_BRIGHTNESS, value
        )
    }

    /**
     * caller should have appropriate permissions
     * @return Int
     */
    fun getBrightness(): Int {
        return Settings.System.getInt(context.contentResolver, Settings.System.SCREEN_BRIGHTNESS)
    }

    /**
     * caller should have appropriate permissions
     * @return Int
     */
    fun getVolume(stream: Int = STREAM_MUSIC): Int {
        require(
            stream in arrayOf(
                STREAM_MUSIC,
                STREAM_SYSTEM,
                STREAM_ALARM,
                STREAM_NOTIFICATION,
                STREAM_RING,
                STREAM_ACCESSIBILITY,
                STREAM_DTMF,
                STREAM_VOICE_CALL
            )
        )
        val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        return audioManager.getStreamVolume(stream)
    }

    fun launchApp(
        packageName: String,
        intExtras: Map<String, Int> = emptyMap(),
        stringExtras: Map<String, String> = emptyMap()
    ) {
        context.packageManager.getLaunchIntentForPackage(packageName)?.run {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP

            intExtras.entries.forEach {
                putExtra(it.key, it.value)
            }

            stringExtras.entries.forEach {
                putExtra(it.key, it.value)
            }

            try {
                context.startActivity(this)
            } catch (ex: ActivityNotFoundException) {
                //ignore
                ex.printStackTrace()
            }
        }

    }


}