package relict007.fullfeaturedlibrary.misc

import io.reactivex.Flowable
import io.reactivex.processors.UnicastProcessor

class UnicastRxQueue<T> {
    private val processor = UnicastProcessor.create<T>()

    fun post(value: T) {
        if (!processor.hasComplete() && !processor.hasThrowable())
            processor.onNext(value)
    }

    fun listen(): Flowable<T> {
        return processor.toSerialized()
    }

    fun destroy() = processor.onComplete()
}