package relict007.fullfeaturedlibrary.misc

import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES.M
import android.os.Looper
import android.util.Log
import relict007.fullfeaturedlibrary.BuildConfig

/**
 * Should only be used for debugging
 */
fun isMainThread(): Boolean {
    return if (SDK_INT >= M) {
        Looper.getMainLooper().isCurrentThread
    } else {
        Thread.currentThread() == Looper.getMainLooper().thread
    }
}

fun ensureMainThread() {
    if (!isMainThread()) error("Not running in main thread")
}

fun printThreadInfo(tag: String = "ThreadInfo") {
    with(Thread.currentThread()) {
        Log.v(
            tag, """

                id: $id
                name: $name
                state: $state


            """
        )
    }
}