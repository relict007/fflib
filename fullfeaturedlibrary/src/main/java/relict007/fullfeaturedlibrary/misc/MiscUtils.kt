package relict007.fullfeaturedlibrary.misc

import android.os.Build
import java.lang.RuntimeException
import java.util.*

/**
 * This is a lambda which does nothing
 * This is used mostly for empty placeholders
 */
object DO_NOTHING : () -> Unit {
    override fun invoke() {
        //do nothing
    }
}

fun Any.ensureEquals(value: Any) {
    if (this != value) throw IllegalArgumentException("this value $this is not equal to given value $value")
}

/**
 * checks value only when receiver is true
 * @receiver Boolean
 * @param value Boolean
 */
fun Boolean.require(value: Boolean) {
    if (this) kotlin.require(value)
}

/**
 * checks value only when receiver is true
 * @receiver Boolean
 * @param value Boolean
 * @param lazyMessage () -> String
 */
inline fun Boolean.require(value: Boolean, lazyMessage: () -> String) {
    if (this) kotlin.require(value, lazyMessage)
}

const val EMPTY_STRING: String = ""

val RANDOM_STRING: String
    get() = UUID.randomUUID().toString()

val isWorkManagerSupported: Boolean
    get() = Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1
