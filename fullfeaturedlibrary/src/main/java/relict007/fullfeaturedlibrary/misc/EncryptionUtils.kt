package relict007.fullfeaturedlibrary.misc

import android.util.Base64
import arrow.core.Option
import arrow.core.Try
import com.tozny.crypto.android.AesCbcWithIntegrity
import java.security.MessageDigest

val String.sha256: String
    get() = hashString("SHA-256", this)

fun String.toBase64(): String {
    return Base64.encodeToString(toByteArray(), Base64.NO_WRAP)
}

fun String.fromBase64(): Option<String> {
    return Try { Base64.decode(this, Base64.NO_WRAP).toString(Charsets.UTF_8) }.toOption()
}

fun String.aesDecrypt(password: String, salt: String): Option<String> {
    return Try {
        AesCbcWithIntegrity.decryptString(
            AesCbcWithIntegrity.CipherTextIvMac(this),
            AesCbcWithIntegrity.generateKeyFromPassword(password, salt)
        )
    }.toOption()
}

private fun hashString(type: String, input: String): String {
    val HEX_CHARS = "0123456789abcdef"
    val bytes = MessageDigest
        .getInstance(type)
        .digest(input.toByteArray())
    val result = StringBuilder(bytes.size * 2)
    bytes.forEach {
        val i = it.toInt()
        result.append(HEX_CHARS[i shr 4 and 0x0f])
        result.append(HEX_CHARS[i and 0x0f])
    }
    return result.toString()
}