package relict007.fullfeaturedlibrary.misc


import androidx.annotation.CallSuper
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.processors.UnicastProcessor
import relict007.fullfeaturedlibrary.BuildConfig
import kotlin.reflect.KClass

interface FsmState
interface FsmEvent

private object Born : FsmEvent
private object NoState : FsmState

data class EventTransition(
    val onFsmEvent: KClass<out FsmEvent>,
    val nextState: KClass<out FsmState>,
    val nextStateBuilder: (event: FsmEvent) -> FsmState
)

data class StateInfo(
    val state: KClass<out FsmState>,
    val transitions: Set<EventTransition>,
    val onEntry: () -> Unit = {},
    val onExit: () -> Unit = {}
)


abstract class Fsm(
    protected val scheduler: Scheduler,
    protected val tag: String = "Fsm",
    private val debug: Boolean = false
) {
    private var _state: FsmState = this.initialState()
    //read only property for child classes
    protected val myState: FsmState
        get() = _state

    private val stateInfoList = mutableListOf<StateInfo>()
    private val stateProcessor = BehaviorProcessor.create<FsmState>().apply { onNext(_state) }
    private val processor = UnicastProcessor.create<FsmEvent>().apply {
        toSerialized()
            .observeOn(scheduler)
            //any error here will crash the app which is what we want
            .subscribe { event ->
                if (event is Born) {
                    stateInfoList.firstOrNull { it.state == initialState()::class }
                        ?.onEntry?.invoke()
                } else {
                    postEventInternal(event)
                }
            }
    }

    /**
     * make sure you call it only once else it will throw an exception
     * @param list List<StateInfo>
     */
    protected fun setStateInfoList(list: List<StateInfo>) {
        assert(stateInfoList.isEmpty())
        stateInfoList.addAll(list)
        //run initial _state entry action
        postEvent(Born)
    }

    protected abstract fun initialState(): FsmState

    private fun postEventInternal(event: FsmEvent) {
        log("processing event ${event.javaClass.simpleName} in _state ${_state.javaClass.simpleName}")
        //find new _state and transition
        val newState =
            stateInfoList.firstOrNull { it.state == _state::class }?.transitions?.firstOrNull { it.onFsmEvent == event::class }?.nextStateBuilder?.invoke(
                event
            ) ?: _state

        if (newState == _state) {
            log("_state ${newState.javaClass.simpleName} same as before, ignoring")
        } else {
            //run exit action of previous _state
            stateInfoList.firstOrNull { it.state == _state::class }?.onExit?.invoke()
            log("_state transition ${_state.javaClass.simpleName} --> ${newState.javaClass.simpleName}")
            _state = newState
            //run entry action
            stateInfoList.firstOrNull { it.state == newState::class }?.onEntry?.invoke()
            //notify _state change
            stateProcessor.onNext(_state)
        }

    }

    private fun log(message: String) {
        if (debug) println("$tag ---> $message")
    }

    fun postEvent(event: FsmEvent) {
        log("posting event ${event.javaClass.simpleName} in _state ${_state.javaClass.simpleName}")
        processor.onNext(event)
    }

    @CallSuper
    open fun destroy() {
        processor.onComplete()
        stateProcessor.onComplete()
        _state = NoState
        stateInfoList.clear()
    }

    @CallSuper
    open fun stateFlowable(): Flowable<FsmState> {
        return stateProcessor.toSerialized()
    }
}


class FsmBuilder<STATE : FsmState, EVENT : FsmEvent> {
    val _data = mutableListOf<StateInfo>()

    inline fun <reified S : STATE> fsmState(noinline init: StateInfoBuilder.() -> Unit): StateInfoBuilder {
        return StateInfoBuilder(S::class).apply(init).apply {
            val stateInfo = build()
            if (BuildConfig.DEBUG) {
                _data.firstOrNull { it.state == stateInfo.state }?.let {
                    throw IllegalArgumentException("state $state already registered with $it")
                }
            }
            _data.add(stateInfo)
        }
    }

    fun build(): List<StateInfo> = _data


    inner class StateInfoBuilder(val state: KClass<out STATE>) {
        private var _onEntry: () -> Unit = {}
        private var _onExit: () -> Unit = {}
        val _transitions = mutableSetOf<EventTransition>()


        fun onEntry(entry: () -> Unit) {
            _onEntry = entry
        }

        fun onExit(exit: () -> Unit) {
            _onExit = exit
        }

        inline fun <reified E : EVENT, reified S : STATE> transition(noinline builder: (event: E) -> S) {
            //check if transition already exists
            if (BuildConfig.DEBUG) {
                _transitions.firstOrNull { it.onFsmEvent == E::class }?.let {
                    throw IllegalArgumentException("event ${E::class} already registered with $it")
                }
            }
            _transitions.add(EventTransition(E::class, S::class) { builder(it as E) })
        }

        fun build(): StateInfo {
            return StateInfo(state, _transitions, _onEntry, _onExit)
        }

    }
}

fun <S : FsmState, E : FsmEvent> fsmBuilder(init: FsmBuilder<S, E>.() -> Unit): FsmBuilder<S, E> {
    return FsmBuilder<S, E>().apply(init)
}