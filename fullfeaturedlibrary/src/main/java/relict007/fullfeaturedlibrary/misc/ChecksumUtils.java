package relict007.fullfeaturedlibrary.misc;


import androidx.annotation.NonNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import androidx.annotation.WorkerThread;
import io.reactivex.Single;

/**
 * Created by saurabh
 */

public class ChecksumUtils {

    @WorkerThread
    public static Single<String> getSha256Sum(@NonNull final File file) {
        return Single.fromCallable(() -> {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            try (InputStream in = new FileInputStream(file)) {
                DigestInputStream dis = new DigestInputStream(in, md);
                byte[] buffer = new byte[4096];
                while (dis.read(buffer) != -1) {
                    //
                }
                dis.close();
            }
            byte[] digest = md.digest();
            return bytesToHex(digest);
        });
    }

    @WorkerThread
    private static String bytesToHex(byte[] bytes) {
        final char[] hexArray = "0123456789abcdef".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
