package relict007.fullfeaturedlibrary.misc

import android.content.Context

/**
 * Created by saurabh
 */


fun Context.getAppName(): String {
    return applicationInfo.loadLabel(packageManager).toString()
}
