package relict007.fullfeaturedlibrary.misc

import android.net.Uri

/*
 * checks if given uri belongs to offline content
 * @param uri
 * @return true if uri is of offline content else false
 */
fun isOfflineUri(uri: String): Boolean {
    return when (Uri.parse(uri).scheme?.toLowerCase()) {
        "file", "asset" -> true
        else -> false
    }
}

fun isAssetUri(uri: String): Boolean {
    return when (Uri.parse(uri)?.scheme?.toLowerCase()) {
        "asset" -> true
        else -> false
    }
}

fun isFileUri(uri: String): Boolean {
    return when (Uri.parse(uri)?.scheme?.toLowerCase()) {
        "file" -> true
        else -> false
    }
}

fun findOfflineUri(uris: List<String>): String? {
    return with (uris.filter { isOfflineUri(it) }
            .partition { isAssetUri(it) }) {
        when {
            first.isNotEmpty() -> first.first()
            else -> second.first()
        }
    }

}

fun isContentUri(uri: String): Boolean {
    return uri.startsWith("content://")
}